using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        private float iPrimero;
        private float iSegundo;
        private float iResultado;
        private String iOperacion;
        
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }


        private void button15_Click(object sender, EventArgs e)
        {
            iOperacion = "*";
            iPrimero = float.Parse(pantalla.Text);
            pantalla.Clear();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            pantalla.Text += "2";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            pantalla.Text += "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            pantalla.Text += "1";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            pantalla.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            pantalla.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            pantalla.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            pantalla.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            pantalla.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            pantalla.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            pantalla.Text += "9";
        }

        private void punto_Click(object sender, EventArgs e)
        {
            pantalla.Text += ".";
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            iOperacion = "+";
            iPrimero = float.Parse(pantalla.Text);
            pantalla.Clear();
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            iOperacion = "/";
            iPrimero = float.Parse(pantalla.Text);
            pantalla.Clear();
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            iOperacion = "-";
            iPrimero = float.Parse(pantalla.Text);
            pantalla.Clear();
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            iSegundo = float.Parse(pantalla.Text);

            switch (iOperacion)
            {
                case "+":
                    iResultado = iPrimero + iSegundo;
                    pantalla.Text = iResultado.ToString();
                    break;
                case "-":
                    iResultado = iPrimero - iSegundo;
                    pantalla.Text = iResultado.ToString();
                    break;
                case "*":
                    iResultado = iPrimero * iSegundo;
                    pantalla.Text = iResultado.ToString();
                    break;
                case "/":
                    iResultado = iPrimero / iSegundo;
                    pantalla.Text = iResultado.ToString();
                    break;
                 }
            {
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            pantalla.Clear();
        }
    }
    }
